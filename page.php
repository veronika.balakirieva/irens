<?php get_header(); ?>

<?php
if( is_front_page() ){
    get_template_part('v-page-home');
} else {
    echo get_template_name($post->ID);
}
?>

<?php get_footer(); ?>