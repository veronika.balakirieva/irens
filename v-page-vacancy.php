<?php /*Template Name: Vacancy */ ?>
<?php get_header(); ?>
<?php $content =  get_field('page'); ?>

    <section class="int-hero">
        <div class="video-bg">
            <video src="<?php echo get_template_directory_uri(); ?>/videos/video.mp4" muted autoplay loop></video>
        </div>
        <div class="inner">
            <h2><?php echo $content['title'];?></h2>
        </div>
    </section>
    <section class="content">
        <div class="journal">
            <div class="post highlight wow fadeInUp">
                <figure>
                    <img src="<?php echo (!empty($content['top']['image']['sizes']['top_img'])?$content['top']['image']['sizes']['top_img']:$content['top']['image']['url']); ?>" alt="<?php echo $content['page_title'];?>">
                    <div class="post-content">
                        <h3><span><?php echo $content['top']['title'];?></span></h3>
                    </div>
                </figure>
            </div>
            <div class="clearfix"></div>
            <?php if ( $content['text'] != '' ){?>
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <?php echo $content['text'];?>
                        <br>
                        <br>
                    </div>
                </div>
            </div>
            <div class="clearfix"></div>
            <?php }?>
            <div class="container">
                <div class="row">
                    <?php $stati_children = new WP_Query(array(
                            'post_type' => 'page',
                            'post_parent' => get_the_ID()
                        )
                    );
                    if($stati_children->have_posts()) :
                        while($stati_children->have_posts()): $stati_children->the_post(); ?>
                            <div class="col-md-6 col-sm-12 wow fadeInUp">
                                <div class="post">
                                    <figure>
                                        <a href="<?php echo get_the_permalink();?>">
                                            <img src="<?php echo the_post_thumbnail_url('post_prev');?>" alt="Image">
                                        </a>
                                    </figure>
                                    <div class="post-content">
                                        <h3>
                                            <a href="<?php echo get_the_permalink();?>"><?php echo get_the_title();?></a>
                                        </h3>
                                        <p><?php echo get_the_excerpt();?></p>
                                        <a href="<?php echo get_the_permalink();?>" class="link"><?php echo pll_e('READ MORE')?></a>
                                    </div>
                                </div>
                            </div>
                        <?php endwhile;
                    endif; wp_reset_query(); ?>
                </div>
            </div>
        </div>
    </section>
</main>
<div class="footer-spacing"></div>
<footer class="footer">
    <?php get_template_part_with_data('templates/v-footer-logo');?>

    <?php $socials = array('media'); get_template_part_with_data('templates/v_social', $socials); ?>

    <?php $footer =  get_field('footer-page'); ?>
    <?php if ( !empty($footer) ) get_template_part_with_data('templates/v_footer', $footer); ?>

<?php get_footer(); ?>