<?php /* Template Name: клиенты/скауты/О нас */ ?>
<?php get_header(); ?>
<?php $content =  get_field('page'); ?>

    <section class="int-hero">
        <div class="video-bg">
            <video src="<?php echo get_template_directory_uri(); ?>/videos/video.mp4" muted autoplay loop></video>
        </div>
        <div class="inner">
            <h2><?php echo $content['page_title'];?></h2>
            <?php if ( $content['subtitle'] != '' ) {?>
            <div class="clearfix"></div>
            <br>
            <p><?php echo $content['subtitle'];?></p>
            <? }?>
        </div>
    </section>

    <section class="content">
        <div class="about-studio">
            <?php if ( !empty($content['image']['url']) ) { ?>
            <figure class="hero-image wow fadeInUp">
                <img src="<?php echo (!empty($content['image']['sizes']['top_img'])?$content['image']['sizes']['top_img']:$content['image']['url']); ?>" alt="<?php echo $content['page_title'];?>">
            </figure>
            <?php }?>

            <?php if ( !empty($content['features']) ) {?>
                <div class="icon-features">
                    <div class="container">
                        <div class="row justify-content-md-center">
                            <?php foreach ($content['features'] as $feature) {?>
                                <div class="col-md-4 col-sm-4 col-12 wow fadeIn mb-60">
                                    <img src="<?php echo (!empty($feature['ico']['sizes']['ico']))?$feature['ico']['sizes']['ico']:$feature['ico']['url'] ?>" alt="<?php echo $feature['title'];?>">
                                    <small><?php echo $feature['title'];?></small>
                                    <p><?php echo $feature['text'];?></p>
                                    <?php if (!empty($feature['link']['url'])){?>
                                        <a data-toggle="modal" data-target="#m1" href="<?php echo $feature['link']['url'];?>"><?php echo $feature['link']['title'];?></a>
                                    <?php }?>
                                </div>
                            <?php }?>
                        </div>
                    </div>
                </div>
            <?php }?>

            <?php
            $footer =  get_field('footer-page', $post->ID);
            if ( !empty($footer['button_name']) && !empty($footer) ) { ?>
                <div class="buttons">
                    <a data-toggle="modal" data-target="#m1" href="#" class="btn-contact">
                        <span data-hover="<?php echo $footer['button_name'];?>"><?php echo $footer['button_name'];?></span>
                    </a>
                </div>
            <?php } ?>

            <div class="container">
                <div class="row">
                    <div class="col-md-4 wow fadeInLeft">
                        <h5><?php echo $content['content']['left']['title'];?></h5>
                        <p class="lead"><?php echo $content['content']['left']['subtitle'];?></p>
                        <?php if ( !empty($content['content']['left']['link']['url']) ) {?>
                        <a href="<?php echo $content['content']['left']['link']['url'];?>" class="link">
                            <?php echo $content['content']['left']['link']['title'];?>
                        </a>
                        <?php }?>
                    </div>
                    <div class="col-md-8 wow fadeInRight">
                        <?php if ( !empty($content['content']['right']['lead']) ) {?>
                        <p class="lead"><?php echo $content['content']['right']['lead'];?></p>
                        <?php }?>
                        <?php echo $content['content']['right']['text'];?>
                    </div>
                </div>
            </div>
        </div>
        <?php if ( !empty($content['members'][0]['photo']) ) {?>
        <div class="team-members wow fadeInUp">
            <?php foreach ($content['members'] as $member) {?>
            <figure class="member">
                <img src="<?php echo (!empty($member['photo']['sizes']['member'])?$member['photo']['sizes']['member']:$member['photo']['url'])?>" alt="<?php echo $member['name']?>">
                <figcaption>
                    <div>
                        <h3><?php echo $member['name']?></h3>
                        <small><?php echo $member['position']?></small>
                    </div>
                </figcaption>
            </figure>
            <?php }?>
        </div>
        <?php }?>

        <?php
        if ($content['add_about_block'][0] === 'add'){
        if ( !empty($content['about']) ){
            $data = [
                $content['about'],
                $footer = get_field('footer-page', $post->ID)
            ];
            get_template_part_with_data('templates/v-about', $data);
        }
        ?>
        <?php if ( !empty($content['client']) ) {?>
            <div class="clients">
                <div class="container">
                    <div class="row">
                        <div class="col-12 wow fadeIn">
                            <ul>
                                <?php foreach ( $content['client'] as $client ) { ?>
                                <li>
                                    <figure>
                                        <img src="<?php echo $client['logo']['sizes']['logo_img'];?>" alt="<?php echo $client['logo']['title'];?>">
                                    </figure>
                                </li>
                                <?php }?>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        <?php }
        } ?>
    </section>
</main>
<div class="footer-spacing"></div>
<footer class="footer">
    <?php get_template_part_with_data('templates/v-footer-logo');?>

    <?php $socials = array('media'); get_template_part_with_data('templates/v_social', $socials); ?>

    <?php $footer =  get_field('footer-page');?>
    <?php if ( !empty($footer) ) get_template_part_with_data('templates/v_footer', $footer); ?>

<?php get_footer(); ?>
