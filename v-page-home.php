<?php /* Template Name: Home */ ?>
<?php get_header(); ?>
<?php $content =  get_field('page'); ?>

    <?php if (!empty($content['slide'])) get_template_part_with_data('templates/v_slider', $content['slide']); ?>

    <section class="content">
        <div id="sc"></div>
        <div class="gallery-wrap">
            <div class="container wow fadeIn">
                <div class="row">
                    <?php $num = 1; foreach ($content['gallery_']['image'] as $galImg) { $num++; $d = $num/6;?>
                    <div class="col-md-4 col-sm-4 col-12 wow fadeIn" data-wow-delay="<?php echo $d;?>s">
                        <img src="<?php echo $galImg['image']['sizes']['home_gal_img'];?>" alt="<?php echo $galImg['image']['title'];?>">
                    </div>
                    <?php }?>
                </div>
                <?php if ( !empty($content['gallery_']['link']['url']) ) {?>
                <div class="row">
                    <a href="<?php echo $content['gallery_']['link']['url'];?>" class="btn-contact wow fadeInDown" data-wow-delay="0.8s">
                        <span data-hover="<?php echo $content['gallery_']['link']['title'];?>"><?php echo $content['gallery_']['link']['title'];?></span>
                    </a>
                </div>
                <?php }?>
            </div>
        </div>
        <div class="icon-features-home">
            <?php if ( !empty($content['feature']) ) get_template_part_with_data('templates/v-features', $content['feature']); ?>
        </div>

        <?php if ( !empty($content['about']) ) {
            $data = [$content['about']];
            get_template_part_with_data('templates/v-about', $data);
        } ?>

        <?php //if ( !empty($content['irens']) ) get_template_part_with_data('templates/v-sect6', $content['irens']); ?>
    </section>
</main>
<div class="footer-spacing"></div>
<footer class="footer">
    <?php get_template_part_with_data('templates/v-footer-logo');?>

    <?php $socials = array('media'); get_template_part_with_data('templates/v_social', $socials); ?>

    <?php $footer =  get_field('footer-page'); ?>
    <?php if ( !empty($footer) ) get_template_part_with_data('templates/v_footer', $footer); ?>

<?php get_footer(); ?>