<?php /* Template Name: Контакты */ ?>
<?php get_header(); ?>
<?php $content =  get_field('page'); ?>

    <section class="int-hero">
        <div class="video-bg">
            <video src="<?php echo get_template_directory_uri(); ?>/videos/video.mp4" muted autoplay loop></video>
        </div>
        <div class="inner">
            <h2><?php echo $content['page_title'];?></h2>
        </div>
    </section>
    <section class="content">
        <div class="contact">
            <div class="container">
                <div class="row justify-content-md-center">
                    <div class="col-12 wow fadeInUp">
                        <div class="titles">
                            <h5><?php echo $content['top']['label'];?></h5>
                            <h2><?php echo $content['top']['title'];?></h2>
                            <p><?php echo $content['top']['subtitle'];?></p>
                        </div>
                    </div>
                    <?php if (  !empty($content['contacts']) ) {?>
                        <?php $num=0; foreach ( $content['contacts'] as $contact ) { $num++;?>
                        <div class="col-md-6 col-sm-6 col-12 wow fadeIn">
                            <div class="address">
                                <h4><?php echo $contact['title'];?></h4>
                                <p>
                                    <?php echo $contact['phone'];?><br>
                                    <?php if ( !empty($contact['email']) ) {?>
                                    <a href="<?php echo $contact['email'];?>"><?php echo $contact['email'];?></a>
                                    <?php }?>
                                    <strong><?php echo $contact['name'];?></strong>
                                    <br>
                                    <?php $contact['address'];?>
                                </p>

                                <?php if ( $contact['telegram'] != '' ) {?>
                                <p>
                                    <a title="Telegram" class="telegram" href="tg://resolve?domain=<?php echo $contact['telegram'];?>">
                                        <svg height="24" viewBox="1 -35 511.99993 511" xmlns="http://www.w3.org/2000/svg">
                                            <path d="m121.453125 253.171875 63.554687 158.886719 82.75-82.753906 141.535157 112.503906 102.707031-441.308594-512 205.480469zm-39.933594-47.640625 244.046875-97.945312-194.074218 117.363281zm287.535157-89.25-161.980469 148.1875-19.484375 73.425781-36.035156-90.085937zm-149.851563 219.230469 9.816406-36.996094 15.144531 12.035156zm171.65625 53.394531-147.386719-117.152344 221.902344-203.007812zm0 0"/>
                                        </svg>
                                        Telegram
                                    </a>
                                </p>
                                <?php }?>
                                <a data-fancybox="" data-src="#map<?php echo $num;?>" href="javascript:;">
                                    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24">
                                        <path d="M12 22.31l-6.38-8.2A7.7 7.7 0 0 1 11.7 1.69h.6a7.7 7.7 0 0 1 6.08 12.42zm-.3-18.62A5.7 5.7 0 0 0 6 9.39a5.77 5.77 0 0 0 1.2 3.5l4.8 6.17 4.8-6.17a5.77 5.77 0 0 0 1.2-3.5 5.7 5.7 0 0 0-5.7-5.7z"></path>
                                        <circle cx="12" cy="9.69" r="2"></circle>
                                    </svg>
                                    GOOGLE MAPS
                                </a>
                            </div>
                        </div>
                    <?php } unset($contact);
                    }?>
                </div>
            </div>
            <?php $num=0; foreach ( $content['contacts'] as $contact ) { $num++; ?>
            <div class="map" id="map<?php echo $num;?>"></div>
            <?php } unset($contact,$num);?>
        </div>
    </section>
</main>

<div class="footer-spacing"></div>
<footer class="footer">
    <?php get_template_part_with_data('templates/v-footer-logo');?>

    <?php $socials = array('media'); get_template_part_with_data('templates/v_social', $socials); ?>

    <?php $footer =  get_field('footer-page');?>
    <?php if ( !empty($footer) ) get_template_part_with_data('templates/v_footer', $footer); ?>

<?php get_footer(); ?>


<script>
<?php $num=0; foreach ( $content['contacts'] as $contact ) { $num++;?>
google.maps.event.addDomListener(window, 'load', init<?php echo $num?>);
function init<?php echo $num?>() {var map = new google.maps.Map(document.getElementById('map<?php echo $num?>'),{center:{lat: <?php echo $contact['lat_lng']['lat']?>, lng: <?php echo $contact['lat_lng']['lng']?> }, zoom: 15, styles: [{featureType:"all",elementType:"geometry.fill",stylers:[{weight:"2.00"}]}, {featureType:"all",elementType:"geometry.stroke",stylers:[{color:"#9c9c9c"}]}, {featureType:"all",elementType:"labels.text",stylers:[{visibility:"on"}]}, {featureType:"landscape",elementType:"all",stylers:[{color:"#f2f2f2"}]}, {featureType:"landscape",elementType:"geometry.fill",stylers:[{color:"#ffffff"}]}, {featureType:"landscape.man_made",elementType:"geometry.fill",stylers:[{color:"#ffffff"}]}, {featureType:"poi",elementType:"all",stylers:[{visibility:"off"}]}, {featureType:"poi.medical",elementType:"geometry.fill",stylers:[{visibility:"on"}, {hue:"#ff0000"},{invert_lightness:!0}]}, {featureType:"poi.medical",elementType:"labels.text",stylers:[{visibility:"simplified"},{invert_lightness:!0}]}, {featureType:"road",elementType:"all",stylers:[{saturation:-100},{lightness:45}]}, {featureType:"road",elementType:"geometry.fill",stylers:[{color:"#eeeeee"}]}, {featureType:"road",elementType:"labels.text.fill",stylers:[{color:"#7b7b7b"}]}, {featureType:"road",elementType:"labels.text.stroke",stylers:[{color:"#ffffff"}]}, {featureType:"road.highway",elementType:"all",stylers:[{visibility:"simplified"}]}, {featureType:"road.arterial",elementType:"labels.icon",stylers:[{visibility:"off"}]}, {featureType:"transit",elementType:"all",stylers:[{visibility:"off"}]}, {featureType:"water",elementType:"all",stylers:[{color:"#46bcec"},{visibility:"on"}]}, {featureType:"water",elementType:"geometry.fill",stylers:[{color:"#c8d7d4"}]}, {featureType:"water",elementType:"labels.text.fill",stylers:[{color:"#070707"}]}, {featureType:"water",elementType:"labels.text.stroke",stylers:[{color:"#ffffff"}]}]});var image = '/wp-content/themes/irens/images/map-pin.svg';var beachMarker = new google.maps.Marker({position:{lat: <?php echo $contact['lat_lng']['lat']?>, lng: <?php echo $contact['lat_lng']['lng']?>}, map:map, icon:image});}
<?php }?>
</script>