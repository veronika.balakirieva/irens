<?php /* Template Name: Галерея */ ?>
<?php get_header(); ?>
<?php $content =  get_field('page'); ?>

    <section class="int-hero">
        <div class="video-bg">
            <video src="<?php echo get_template_directory_uri(); ?>/videos/video.mp4" muted autoplay loop></video>
        </div>
        <div class="inner">
            <h2><?php echo $content['page_title'];?></h2>
        </div>
    </section>

    <section class="content content-gallery">
        <div class="row">
            <?php $num = 1; foreach ($content['gallery'] as $gallery){ $num++; $d = $num/6;?>
            <div class="col-md-3 col-sm-4 col-12 wow fadeIn" data-wow-delay="<?php echo $d;?>s">
                <a data-fancybox="gallery" href="<?php echo $gallery['image']['url']?>">
                    <img src="<?php echo $gallery['image']['sizes']['gal_img']?>" alt="<?php echo $gallery['image']['title']?>">
                </a>
            </div>
            <?php }?>
        </div>
    </section>
</main>
<div class="footer-spacing"></div>
<footer class="footer">
    <?php get_template_part_with_data('templates/v-footer-logo');?>

    <?php $socials = array('media'); get_template_part_with_data('templates/v_social', $socials); ?>

    <?php $footer =  get_field('footer-page');?>
    <?php if ( !empty($footer) ) get_template_part_with_data('templates/v_footer', $footer); ?>

<?php get_footer(); ?>