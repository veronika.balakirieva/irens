<?php get_header(); ?>
<link rel='stylesheet' href='<?php echo get_template_directory_uri(); ?>/styles/404.css' media='all' />
<div class="not-page">
    <div class="item">
        <div class="not-page__title">404</div>
        <div class="not-page__subtitle">
            <?php pageText('page_404')?>
        </div>
        <a href="<?php echo esc_url(home_url('/')); ?>" class="not-page__link"><?php pageText('Go_to_home')?></a>
    </div>
    <div class="img-container">
        <img src="<?php echo get_template_directory_uri(); ?>/styles/imgs/404-image.png" alt="image">
    </div>
</div>

<?php get_footer(); ?>
