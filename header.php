<!DOCTYPE html>
<html lang="en-US">
<head>
    <meta charset="<?php bloginfo('charset'); ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="format-detection" content="telephone=no">
    <title><?php
        global $page, $paged;
        wp_title('|', true, 'right');
        bloginfo('name');
        $site_description = get_bloginfo('description', 'display');
        if ($site_description && (is_home() || is_front_page()))
            echo " | $site_description";
        if ($paged >= 2 || $page >= 2)
            echo ' | ' . sprintf(__('page %s', 'twentyten'), max($paged, $page));
        ?></title>

    <style>
        #loader{transition:opacity 400ms;}
        body.loaded #loader{opacity:0;pointer-events:none;}
        #loader{position:fixed;display:block;width:100%;height:100%;top:0;left:0;z-index:111;background:#020202}
        .loading{position:absolute;display:block;width:12px;height:12px;top:0;left:0;right:0;bottom:0;margin:auto}
        .loading svg{position:absolute;display:block;width:120px;overflow:visible!important;left:-100px;right:-100px;top:-30px;margin:auto;}
        .loading path,.loading g{transform-origin:50% 50%;}
        .loading .white-path{fill:#fff}
        .loading .orange-path{fill:#f50c1a}
        .loading .ltr1{animation:loadLetterOrange 500ms ease-out 0ms infinite alternate}
        .loading .ltr2{animation:loadLetterWhite 500ms ease-out 60ms infinite alternate}
        .loading .ltr3{animation:loadLetterWhite 500ms ease-out 120ms infinite alternate}
        .loading .ltr4{animation:loadLetterWhite 500ms ease-out 180ms infinite alternate}
        .loading .ltr5{animation:loadLetterWhite 500ms ease-out 240ms infinite alternate}
        .loading .ltr6{animation:loadLetterWhite 500ms ease-out 320ms infinite alternate}
        @keyframes loadLetterWhite {
            0%{transform:translate3d(8px,0,0) scale(1)}
            20%{fill:#fff}
            60%{fill:#fff}
            80%{fill:#f50c1a}
            100%{transform:translate3d(-5px,0,0) scale(1.35)}
        }
        @keyframes loadLetterOrange {
            0%{transform:translate3d(8px,0,0) scale(1)}
            20%{fill:#f50c1a}
            60%{fill:#f50c1a}
            80%{fill:red}
            100%{transform:translate3d(-5px,0,0) scale(1.35)}
        }
        #cursor .cir .clc,#cursor .cir .hvr,#cursor .dot{background: no-repeat center center url('<?php echo get_template_directory_uri(); ?>/images/irens_small.png');}
        #cursor.white .cir .clc,#cursor.white .cir .hvr,#cursor.white .dot{background: no-repeat center center url('<?php echo get_template_directory_uri(); ?>/images/irens_small-w.png');}
    </style>
    <link href="<?php echo get_template_directory_uri(); ?>/ico/apple-touch-icon-144-precomposed.png" rel="apple-touch-icon" sizes="144x144">
    <link href="<?php echo get_template_directory_uri(); ?>/ico/apple-touch-icon-114-precomposed.png" rel="apple-touch-icon" sizes="114x114">
    <link href="<?php echo get_template_directory_uri(); ?>/ico/apple-touch-icon-72-precomposed.png" rel="apple-touch-icon" sizes="72x72">
    <link href="<?php echo get_template_directory_uri(); ?>/ico/apple-touch-icon-57-precomposed.png" rel="apple-touch-icon">
    <link href="<?php echo get_template_directory_uri(); ?>/ico/favicon.png" rel="shortcut icon">
    <script>window.__openTime = Date.now();</script>


    <?php wp_head(); ?>
    <?php the_field('before_head', 'option'); ?>
</head>
<body>

    <?php /*
    <div class="preloader">
        <img src="<?php echo get_template_directory_uri(); ?>/images/preloader.gif" alt="Image">
        <ul class="text-rotater">
            <li>Hangin there</li>
            <li>Still loading</li>
            <li>Almost done</li>
        </ul>
    </div>
    */?>

<div id="loader">
    <div class="loading">
        <svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 1920 1080" xml:space="preserve">
        <g class="white-path ltr1">
            <polygon points="502.05,572.81 414.27,639.68 502.05,706.55 589.82,639.68"/>
            <polygon points="414.82,506.35 327.05,573.23 414.82,640.1 502.6,573.23"/>
            <polygon points="589.27,506.35 501.49,573.23 589.27,640.1 677.05,573.23"/>
            <polygon points="502.05,439.9 414.27,506.77 502.05,573.65 589.82,506.77"/>
            <polygon points="414.82,373.45 327.05,440.32 414.82,507.19 502.6,440.32"/>
            <polygon points="589.27,373.45 501.49,440.32 589.27,507.19 677.05,440.32"/>
            <polygon points="589.07,506.77 676.84,573.65 676.84,439.9"/>
            <polygon points="414.85,506.77 327.08,439.9 327.08,573.65"/>
        </g>
            <g>
                <path class="white-path ltr2" d="M755.68,706.48V373.52h39.78v332.96H755.68z"/>
                <path class="white-path ltr3" d="M966.94,573.77h-60.08v132.71h-39.78V373.52h142.83l46.15,31.87V541.9l-46.15,31.87l64.06,132.71h-44.16L966.94,573.77z M906.86,416.33v114.63h109.41V416.33H906.86z"/>
                <path class="white-path ltr4" d="M1167.46,556.65v107.02h118.96v42.81h-158.74V373.52h158.74v42.81h-118.96v97.51h95.09v42.81H1167.46z"/>
                <path class="white-path ltr5" d="M1381.89,454.86v251.62h-39.79V373.52h36.21l144.02,251.62h0.8V373.52h39.79v332.96h-36.2l-144.02-251.62H1381.89z"/>
                <path class="white-path ltr6" d="M1634.53,530.49V403.01l46.15-31.87h92.7l46.15,31.87V452h-39.79v-38.05h-105.43v100.84h99.07l46.15,31.87v130.33l-46.15,31.87h-96.68l-46.15-31.87V628h39.79v38.05h109.41V562.36h-99.07L1634.53,530.49z"/>
            </g>
    </svg>

    </div>
</div>
<div class="transition-overlay"></div>
<main>
    <ul class="hamburger-navigation">
        <?php if (pll_current_language() == 'en') { $mName = 'mMenu_en'; }
        if (pll_current_language() == 'no') { $mName = 'mMenu'; }
        wp_nav_menu( array(
            'menu'            => $mName,
            'container'       => false,
            'fallback_cb'     => 'wp_page_menu',
            'items_wrap'      => '%3$s',
            'depth'           => 0,
            'walker'          => '',
        ));?>
        <?php $socials = get_field('social', 'option');?>
        <li class="socials">
            <?php foreach ($socials as $social) { ?>
            <a href="<?php echo $social['url'];?>" target="_blank" rel="nofollow">
                <?php if ( !empty($social['ico']['url']) ) {?>
                    <img src="<?php echo $social['ico']['url']?>" alt="<?php echo $social['name'];?>">
                <?php } else {?>
                    <?php echo $social['name'];?>
                <?php }?>
            </a>
            <?php }?>
        </li>
    </ul>
    <?php /*
    <ul class="hamburger-navigation">
        <li><a href="index.html">Home</a>
            <ul class="dropdown">
                <li><a href="index-video.html">VIDEO BG</a></li>
                <li><a href="index-mouse-split.html">MOUSE SPLIT</a></li>
                <li><a href="index-animation.html">ANIMATION</a></li>
            </ul>
        </li>
        <li><a href="anchor.html">Anchor</a></li>
        <li><a href="showcase-four-cols.html">Showcase</a>
            <ul class="dropdown">
                <li><a href="showcase-two-cols.html">TWO COLS</a></li>
                <li><a href="showcase-three-cols.html">THREE COLS</a></li>
                <li><a href="showcase-four-cols.html">FOUR COLS</a></li>
                <li><a href="showcase-five-cols.html">FIVE COLS</a></li>
            </ul>
        </li>
        <li><a href="journal.html">Journal<span>BETA</span></a></li>
        <li><a href="say-hello.html">Say Hello</a></li>
    </ul>
    <!-- end hamburger-navigation -->
    */ ?>
    <svg class="shape-overlays" viewBox="0 0 100 100" preserveAspectRatio="none">
        <path class="shape-overlays__path" d=""></path>
        <path class="shape-overlays__path" d=""></path>
        <path class="shape-overlays__path" d=""></path>
    </svg>
    <header class="header" id="header">
        <div class="logo">
            <a href="<?php echo esc_url(home_url('/')); ?>">
                <img src="<?php echo get_template_directory_uri(); ?>/images/logoIrens.svg" alt="IRENS" height="40">
            </a>
        </div>
        <div class="hamburger" id="hamburger">
            <div class="hamburger__line hamburger__line--01">
                <div class="hamburger__line-in hamburger__line-in--01"></div>
            </div>
            <div class="hamburger__line hamburger__line--02">
                <div class="hamburger__line-in hamburger__line-in--02"></div>
            </div>
            <div class="hamburger__line hamburger__line--03">
                <div class="hamburger__line-in hamburger__line-in--03"></div>
            </div>
            <div class="hamburger__line hamburger__line--cross01">
                <div class="hamburger__line-in hamburger__line-in--cross01"></div>
            </div>
            <div class="hamburger__line hamburger__line--cross02">
                <div class="hamburger__line-in hamburger__line-in--cross02"></div>
            </div>
        </div>
        <!-- end hamburger -->
        <div class="equalizer">
            <span></span><span></span><span></span><span></span>
        </div>
        <!-- end equalizer -->

        <ul class="language">
            <?php pll_the_languages(); ?>
        </ul>
    </header>

    <?php $socials = array('bar');
    get_template_part_with_data('templates/v_social', $socials); ?>



