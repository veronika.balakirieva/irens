<?php $token = "?".date('YmdH')."&ver=1.1.1";?>
<link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/css/animate.min.css<?php echo $token;?>">
<link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/css/hamburger-menu.css<?php echo $token;?>">
<link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/css/odometer.min.css<?php echo $token;?>">
<link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/css/swiper.min.css<?php echo $token;?>">
<link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/css/fancybox.min.css<?php echo $token;?>">
<link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/css/bootstrap.min.css<?php echo $token;?>">
<link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/css/style.css<?php echo $token;?>">

<script src="<?php echo get_template_directory_uri(); ?>/js/jquery.min.js<?php echo $token;?>"></script>
<script src="<?php echo get_template_directory_uri(); ?>/js/bootstrap.min.js<?php echo $token;?>"></script>
<script src="<?php echo get_template_directory_uri(); ?>/js/swiper.min.js<?php echo $token;?>"></script>
<script src="<?php echo get_template_directory_uri(); ?>/js/fancybox.min.js<?php echo $token;?>"></script>
<script src="<?php echo get_template_directory_uri(); ?>/js/jquery.stellar.js<?php echo $token;?>"></script>
<script src="<?php echo get_template_directory_uri(); ?>/js/odometer.min.js<?php echo $token;?>"></script>
<script src="<?php echo get_template_directory_uri(); ?>/js/hamburger.min.js<?php echo $token;?>"></script>
<script src="<?php echo get_template_directory_uri(); ?>/js/easings.js<?php echo $token;?>"></script>
<script src="<?php echo get_template_directory_uri(); ?>/js/isotope.min.js<?php echo $token;?>"></script>
<script src="<?php echo get_template_directory_uri(); ?>/js/wow.min.js<?php echo $token;?>"></script>
<script src="<?php echo get_template_directory_uri(); ?>/js/perspective.min.js<?php echo $token;?>"></script>
<script>(function($) {$(window).load(function(){$("body").addClass("page-loaded");});})(jQuery)</script>
<script src="<?php echo get_template_directory_uri(); ?>/js/jquery.sticky.js<?php echo $token;?>"></script>
<script src="<?php echo get_template_directory_uri(); ?>/js/scripts.js<?php echo $token;?>"></script>
<?php if ( is_page_template('v-page-contact.php') ) {?>
<script src="<?php echo get_template_directory_uri(); ?>/js/contact.form.min.js<?php echo $token;?>"></script>
<script src="<?php echo get_template_directory_uri(); ?>/js/jquery.form.min.js<?php echo $token;?>"></script>
<script src="<?php echo get_template_directory_uri(); ?>/js/jquery.validate.min.js<?php echo $token;?>"></script>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAAusM7J96858QvL7TxPbu4L3hgqpN-JKg"></script>
<?php }?>
<script>
    /* <![CDATA[ */
    var wpcf7 = {"apiSettings":{"root":"<?php echo wp_parse_url( get_site_url(), PHP_URL_SCHEME )?>:\/\/<?php echo wp_parse_url( get_site_url(), PHP_URL_HOST )?>\/wp-json\/contact-form-7\/v1","namespace":"contact-form-7\/v1"}};
    /* ]]> */
</script>
<script defer src='<?php echo get_template_directory_uri(); ?>/js/cf7.js<?php echo $token;?>'></script>

<?php the_field('before-body', 'option'); ?>

    <link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/css/cursor.css<?php echo $token;?>">

    <div id="cursor">
        <span class="cir" data-num="0">
            <span class="clc"></span>
            <span class="hvr"></span>
        </span>
        <span class="dot" data-num="1"></span>
        <span class="dot" data-num="2"></span>
        <span class="dot" data-num="3"></span>
        <span class="dot" data-num="4"></span>
        <span class="dot" data-num="5"></span>
    </div>
