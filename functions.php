<?php

if( 'disable_gutenberg' ){
    add_filter( 'use_block_editor_for_post_type', '__return_false', 100 );

    // Move the Privacy Policy help notice back under the title field.
    add_action( 'admin_init', function(){
        remove_action( 'admin_notices', [ 'WP_Privacy_Policy_Content', 'notice' ] );
        add_action( 'edit_form_after_title', [ 'WP_Privacy_Policy_Content', 'notice' ] );
    } );
}

if(!function_exists('sgarecat_setup')) {
	function sgarecat_setup() {
		// Add Post Thumbnails Support and Related Image Sizes
		add_theme_support('post-thumbnails');
		add_image_size('icon', 30, 30, true);
		add_image_size('ico', 60, 60, true);
		add_image_size('member', 363, 363, true);
		add_image_size('post_prev', 521, 347, true);
		add_image_size('top_img', 1102, 540, true);
		add_image_size('logo_img', 172, 9999);
		add_image_size('gal_img', 417, 297, true);
		add_image_size('home_gal_img', 350, 290, true);
		/*
		 * Let WordPress manage the document title.
		 * By adding theme support, we declare that this theme does not use a
		 * hard-coded <title> tag in the document head, and expect WordPress to
		 * provide it for us.
		 */
		add_theme_support('title-tag');

		// This theme uses post format support.
		//add_theme_support('post-formats', array(
		//	'aside', 'image', 'video', 'quote', 'link', 'gallery', 'status', 'audio', 'chat'
		//));

		// Add default posts and comments RSS feed links to head
		add_theme_support('automatic-feed-links');

		add_theme_support('menus');

		// Remove tag wp_gead()
		remove_action('wp_head', 'feed_links_extra', 3);
		remove_action('wp_head', 'feed_links', 2);
		remove_action('wp_head', 'rsd_link');
		remove_action('wp_head', 'wlwmanifest_link' );
		remove_action('wp_head', 'index_rel_link' );
		remove_action('wp_head', 'parent_post_rel_link', 10, 0);
		remove_action('wp_head', 'start_post_rel_link', 10, 0);
		remove_action('wp_head', 'adjacent_posts_rel_link', 10, 0);
		remove_action('wp_head', 'wp_generator');
		remove_action('wp_head', 'print_emoji_detection_script', 7);
		remove_action('wp_print_styles', 'print_emoji_styles');
		remove_action('wp_head', 'rest_output_link_wp_head');
		remove_action('wp_head','qtranxf_wp_head_meta_generator');
	}
	add_action('after_setup_theme', 'sgarecat_setup');
}


// Include CSS
function theme_css_js() {
    //wp_deregister_style ('mfcf7_zl_button_style-css');
    wp_deregister_style ('contact-form-7');
    wp_deregister_style ('wp-block-library');
    wp_deregister_style ('mfcf7_zl_button_style');
    wp_deregister_style ('mfcf7-zl-load-fa');
    wp_deregister_style ('fts-feeds');

    wp_deregister_script('contact-form-7');

    wp_deregister_script('jquery'); // Remove jQuery WordPress
	wp_enqueue_script('jquery', esc_url(get_template_directory_uri()) . '/js/common.min.js', false, '0.1', true);
    wp_enqueue_script('jquery');
    wp_enqueue_script('build-min', esc_url(get_template_directory_uri()) . '/js/build.min.js', false, '1.2', true);
}
add_action('wp_enqueue_scripts', 'theme_css_js');


//feed-them-social - instagram only on the page "about us"
function portfolio_scripts_styles() {
    if ( !is_page('10') && !is_page('270') ){
        wp_deregister_script('fts_powered_by_js');
        wp_deregister_script('fts-global');
        wp_deregister_style ('fts-feeds');
    }
}
add_action( 'wp_enqueue_scripts', 'portfolio_scripts_styles' );

// defer
add_filter('script_loader_tag', 'add_async_attribute', 10, 2);
function add_async_attribute($tag, $handle){
    if(!is_admin()){
        if ('jquery-core' == $handle) {
            return $tag;
        }
        return str_replace(' src', ' defer src', $tag);
    }else{
        return $tag;
    }
}


// Remove tag type scripts/tyles
add_filter('style_loader_tag', 'clean_style_tag');
function clean_style_tag($src) {
    return str_replace("type='text/css'", '', $src);
}
add_filter('script_loader_tag', 'clean_script_tag');
function clean_script_tag($src) {
    return str_replace("type='text/javascript'", '', $src);
}

function get_template_part_with_data($slug, array $data = array()){
    $slug .= '.php';
    extract($data);

    require locate_template($slug);
}


function get_thumb_url( $post = null, $size = null ) {
    $post_thumbnail_id = get_post_thumbnail_id( $post );
    if ( ! $post_thumbnail_id ) {
        return false;
    }

    if ( $size == null ){
        $size = 'thumb_news';
    }

    return wp_get_attachment_image_url( $post_thumbnail_id, $size );
}


/**
 * DEBUG
 */
function debug($arr){
    echo '<pre>'. print_r($arr, true) .'</pre>';
}

//get_template_part('theme-options');


add_filter('navigation_markup_template', 'my_navigation_markup_template');
function my_navigation_markup_template() {
    return '%3$s';
}
function themename_the_post_navigation( $args = array() ) {
    if (pll_current_language() == 'en') { $post_back = 'Back';  $post_next = 'Next';}
    if (pll_current_language() == 'no') { $post_back = 'Tilbake'; $post_next = 'Neste'; }
    $args = wp_parse_args( $args, array(
        'prev_text'          => '<img src="'.get_bloginfo('stylesheet_directory').'/styles/imgs/back.png" alt="back" /> '.$post_back,
        'next_text'          => $post_next.' <img src="'.get_bloginfo('stylesheet_directory').'/styles/imgs/back.png" alt="next" />',
        'in_same_term'       => false,
        'excluded_terms'     => '',
        'taxonomy'           => 'category',
        'screen_reader_text' => '',
    ) );

    $navigation = '';

    $previous = get_previous_post_link(
        '<div class="nav-previous">%link</div>',
        $args['prev_text'],
        $args['in_same_term'],
        $args['excluded_terms'],
        $args['taxonomy']
    );

    $next = get_next_post_link(
        '<div class="nav-next">%link</div>',
        $args['next_text'],
        $args['in_same_term'],
        $args['excluded_terms'],
        $args['taxonomy']
    );

    // Only add markup if there's somewhere to navigate to.
    if ( $previous || $next ) {
        $navigation = _navigation_markup( $previous . $next, 'post-navigation', $args['screen_reader_text'] );
    }

    return $navigation;
}


remove_action( 'wp_head', '_wp_render_title_tag', 1 );



/**
 * get children ul
 */
function get_children_ul($postId, $args = Null, $currentPageId = Null){
    if ($postId){
        $parentId = top_level_parent_id($postId);
        $attachments = get_children( array( 'post_type'=>'page', 'post_parent'=>$parentId ) );
        if ($attachments){
            if ( $args ){
                $li = $args['title'];
            } else {
                $li = '';
            }
            if ( !empty($args['subtitle']) ){
                foreach ($args['subtitle'] as $key => $args2){
                    $li .= '<li class="subtitle">'.$key.'</li>';
                    foreach ($args2 as $arg){
                        foreach ( $attachments as $attachment ){
                            if ( $arg == $attachment->post_title) {
                                $class = ($args['currentId'] == $attachment->ID) ? ' class="current"' : '';
                                $li .= '<li><a '. $class .' href="'.$attachment->guid.'">'.$attachment->post_title.'</a></li>';
                            }
                        }
                    }
                }
            }
        }

        $ul = '<ul>'.$li.'</ul>';
        echo $ul;
    }
}

/**
 * get template name
 */
function get_template_name($postId, $getTemplate = Null){
    if ( $postId ) {
        $get_template_name =  get_page_template_slug( top_level_parent_id($postId) );
        $get_template_name = explode('.', $get_template_name);
        $get_template_name = $get_template_name[0];

        if ($getTemplate){

        }
        if ($get_template_name === 'v-page-policy') {
            get_template_part('template-parts/page', 'policy');
        }
        if ($get_template_name === 'v-page-vacancy') {
            get_template_part('templates/v_vacancy');
        }
        //return $get_template_name;
    }
}


/**
 * top_level_parent_id
 */
function top_level_parent_id($postId){
    if ($postId) {
        $ancestors = get_post_ancestors($postId);
        $root = count($ancestors)-1;
        $parent = $ancestors[$root];
    } else {
        $parent = $postId;
    }
    return $parent;
}


function filter_plugin_updates( $update ) {
    global $DISABLE_UPDATE;
    if( !is_array($DISABLE_UPDATE) || count($DISABLE_UPDATE) == 0 ){  return $update;  }
    foreach( $update->response as $name => $val ){
        foreach( $DISABLE_UPDATE as $plugin ){
            if( stripos($name,$plugin) !== false ){
                unset( $update->response[ $name ] );
            }
        }
    }
    return $update;
}
add_filter( 'site_transient_update_plugins', 'filter_plugin_updates' );


add_filter( 'excerpt_length', function(){
    return 18;
} );


if( function_exists('acf_add_options_page') ) {
    acf_add_options_page(array(
        'page_title' 	=> 'Theme General Settings',
        'menu_title'	=> 'Theme Settings',
        'menu_slug' 	=> 'theme-general-settings',
        'capability'	=> 'edit_posts',
        'redirect'		=> false
    ));
    acf_add_options_sub_page(array(
        'page_title' 	=> 'Theme Header Settings',
        'menu_title'	=> 'Header',
        'parent_slug'	=> 'theme-general-settings',
    ));
    acf_add_options_sub_page(array(
        'page_title' 	=> 'Theme Footer Settings',
        'menu_title'	=> 'Footer',
        'parent_slug'	=> 'theme-general-settings',
    ));
    acf_add_options_sub_page(array(
        'page_title' 	=> 'Social',
        'menu_title'	=> 'Social',
        'parent_slug'	=> 'theme-general-settings',
    ));
}

add_filter( 'wpcf7_form_class_attr', 'custom_custom_form_class_attr' );
function custom_custom_form_class_attr( $class ) {
    $class .= ' row inner';
    return $class;
}

pll_register_string('prev', 'PREV', 'true');
pll_register_string('next', 'NEXT', 'true');
pll_register_string('READ MORE', 'READ MORE', 'true');