<div class="icon-features">
    <div class="container">
        <div class="row">
            <?php $num = 0; foreach ($data as $feature) { $num++?>
            <div class="col-md-4 col-sm-4 col-12 wow fadeIn" <?php if($num==2) echo 'data-wow-delay="0.2s"'; if($num==3) echo 'data-wow-delay="0.4s"'?>>
                <img src="<?php echo $feature['ico']['url']; ?>" alt="Image">
                <small><?php echo $feature['name']; ?></small>
                <p><?php echo $feature['text']; ?></p>
                <?php if ( !empty($feature['link']['url']) ) {?>
                <a href="<?php echo $feature['link']['url'];?>"><?php echo $feature['link']['title'];?></a>
                <?php }?>
            </div>
            <?php } unset($num);?>
        </div>
    </div>
</div>