<h4><?php echo $data['label'];?></h4>
<h2><?php echo $data['title'];?></h2>
<?php
$form = $data['form'];
if ( !empty($data['button_name']) && !empty($form) ) { ?>
    <a data-toggle="modal" data-target="#m1" href="#" class="btn-contact">
        <span data-hover="<?php echo $data['button_name'];?>"><?php echo $data['button_name'];?></span>
    </a>
<?php }
$form2 = $data['form2'];
if ( !empty($data['second_button']) && !empty($data['button_name_2']) && !empty($form2) ) { ?>
    <a data-toggle="modal" data-target="#m2" href="#" class="btn-contact">
        <span data-hover="<?php echo $data['button_name_2'];?>"><?php echo $data['button_name_2'];?></span>
    </a>
<?php }?>


<div class="footer-bar">
    <span class="pull-left">
        © <?php echo date('Y'); echo (date('Y')>'2019')?' - '.date('Y'):'';?>
        <?php if (pll_current_language() == 'en') { the_field('copyright', 'option'); }
        if (pll_current_language() == 'ru') { the_field('copyright_ru', 'option'); } ?>
    </span>
    <span class="pull-right">
        <?php if (pll_current_language() == 'en') { the_field('site_created', 'option'); }
        if (pll_current_language() == 'ru') { the_field('site_created_ru', 'option'); } ?>
        <a id="melon" target="_blank" rel="nofollow" href="https://joker.expert/">Joker D&D</a>
    </span>
</div>
</footer>
<audio id="link" src="<?php echo get_template_directory_uri(); ?>/audio/link.mp3" preload="auto"></audio>


<div class="modal fade" id="m1" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
            <div class="modal-body">
                <div class="contact">
                    <?php echo do_shortcode('[cf7form cf7key="'.$form->post_name.'"]');?>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="m2" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
            <div class="modal-body">
                <div class="contact">
                    <?php echo do_shortcode('[cf7form cf7key="'.$form2->post_name.'"]');?>
                </div>
            </div>
        </div>
    </div>
</div>