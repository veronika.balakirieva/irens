<div class="works four-cols">
    <div class="grid-sizer"></div>
    <?php $num = 0; foreach ($data as $work) { $num++;?>
    <div class="<?php if ($num==1 || $num==2 || $num==6) {?>grid-item-double<?php } else {?>grid-item<?php }?>">
        <figure class="reveal-effect se2-white wow perspective-box">
            <img src="<?php echo $work['image']['url']; ?>" alt="Image">
            <figcaption>
                <a href="<?php echo $work['link']['url']; ?>">
                    <div class="bg-color" data-background="#2095f4"></div>
                    <div class="brand">
                        <img src="<?php echo $work['logo']['url']; ?>" alt="Image">
                    </div>
                    <?php if ( !empty($work['name']) ) {?>
                    <h5><?php echo $work['name']; ?></h5>
                    <?php }?>
                    <?php if ( !empty($work['label']) ) {?>
                    <small><?php echo $work['label']; ?></small>
                    <?php }?>
                </a>
            </figcaption>
        </figure>
    </div>
    <?php } unset($num);?>
</div>