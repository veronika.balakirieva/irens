<div class="awards" id="counter">
    <div class="container">
        <div class="row">
            <div class="col-md-5 col-sm-12 col-12 wow fadeIn">
                <h5><?php echo $data['label']?></h5>
                <h2><?php echo $data['name']?></h2>
            </div>
            <div class="col-md-7 col-sm-12 col-12 wow fadeIn" data-wow-delay="0.2s">
                <div class="row inner">
                    <?php $num=0; foreach ($data['nums'] as $nums) { $num++?>
                        <div class="col-md-4 col-sm-4 col-12">
                            <div class="award">
                                <div class="figure">
                                    <?php echo $nums['text'];?>
                                </div>
                                <span class="odometer" id="<?php echo $num;?>" data-id="<?php echo $nums['number'];?>">00</span>
                            </div>
                        </div>
                    <?php }?>
                    <?php /*
                    <div class="col-md-4 col-sm-4 col-12">
                        <div class="award">
                            <figure>
                                <img src="<?php echo get_template_directory_uri(); ?>/images/award01.png" alt="Image">
                            </figure>
                            <span class="odometer" id="1">00</span>
                        </div>
                    </div>
                    <div class="col-md-4 col-sm-4 col-12">
                        <div class="award">
                            <figure>
                                <img src="<?php echo get_template_directory_uri(); ?>/images/award02.png" alt="Image">
                            </figure>
                            <span class="odometer" id="2">00</span>
                        </div>
                    </div>
                    <div class="col-md-4 col-sm-4 col-12">
                        <div class="award">
                            <figure>
                                <img src="<?php echo get_template_directory_uri(); ?>/images/award03.png" alt="Image">
                            </figure>
                            <span class="odometer" id="3">00</span>
                        </div>
                    </div>
                    */ ?>
                </div>
            </div>
        </div>
    </div>
</div>