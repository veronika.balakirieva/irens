<section class="slider">
    <div class="swiper-container">
        <div class="swiper-wrapper">
            <?php foreach ($data as $slide) {?>
            <div class="swiper-slide bg-image" data-background="<?php echo $slide['image']['url']; ?>" data-stellar-background-ratio="0.5">
                <div class="inner">
                    <?php if(!empty($slide['label'])){?><h5 data-swiper-parallax="-600"><?php echo $slide['label']; ?>&nbsp;</h5><?php }?>
                    <?php if(!empty($slide['name'])){?><h2 data-swiper-parallax="-400"><?php echo $slide['name']; ?>&nbsp;</h2><?php }?>
                    <?php if(!empty($slide['text'])){?><p data-swiper-parallax="-200"><?php echo $slide['text']; ?>&nbsp;</p><?php }?>
                    <?php if(!empty($slide['link']['url'])){?>
                    <a href="<?php echo $slide['link']['url']; ?>" class="link">
                        <?php echo $slide['link']['title']; ?>
                    </a>
                    <?php }?>
                </div>
            </div>
            <?php } ?>
        </div>
        <div class="swiper-pagination"></div>
        <div class="swiper-button-prev"><span>
                <?php echo pll_e('PREV')?>
            </span><img src="<?php echo get_template_directory_uri(); ?>/images/arrow-left.svg" alt="Image"></div>
        <span class="swiper-button-line"></span>
        <div class="swiper-button-next"><span>
                <?php echo pll_e('NEXT')?>
            </span><img src="<?php echo get_template_directory_uri(); ?>/images/arrow-right.svg" alt="Image"></div>
    </div>
    <div class="scroll-down"><span></span></div>
</section>