    <?php $content = get_field('page');?>

    <?php if ( !empty($content['top_image']['url']) ) {?>
    <section class="int-hero">

            <div class="video-bg">
                <img src="<?php echo (!empty($content['top_image']['url']))
                    ? $content['top_image']['url']
                    : get_template_directory_uri().'/images/works02.jpg' ?>" alt="Image">
            </div>
            <div class="inner">
                <h2><?php the_title();?></h2>
            </div>

    </section>
    <?php } else {?>
    <section class="int-hero">
        <div class="video-bg">
            <video src="<?php echo get_template_directory_uri(); ?>/videos/video.mp4" muted autoplay loop></video>
        </div>
        <div class="inner">
            <h2><?php the_title();?></h2>
        </div>
    </section>
    <?php }?>

    <section class="content">
        <div class="project-detail">
            <div class="container">
                <? //debug($content['vacancy']) ?>
                <?php if ( !empty($content['vacancy']) ) {?>
                <div class="project-navbar">
                    <ul class="navbar">
                        <?php $num=0; foreach ($content['vacancy'] as $vacancy ){ $num++?>
                        <li><a href="v-<?php echo $num;?>"><?php echo $vacancy['title'];?></a></li>
                        <?php } unset($vacancy, $num);?>
                    </ul>
                </div>
                <?php $num=0; foreach ($content['vacancy'] as $vacancy ){ $num++?>
                <div class="project" id="v-<?php echo $num;?>">
                    <h2><?php echo $vacancy['subtitle'];?></h2>
                    <?php echo $vacancy['content'];?>
                </div>
                <?php }
                }?>

                <?php if ( $content['offer'] != '' ) {?>
                <div class="project mt-60">
                    <?php echo $content['offer'];?>
                </div>
                <?php }?>

                <?php
                $footer =  get_field('footer-page', $post->ID);
                if ( !empty($footer['button_name']) && !empty($footer) ) { ?>
                    <div class="buttons">
                        <a data-toggle="modal" data-target="#m1" href="#" class="btn-contact">
                            <span data-hover="<?php echo $footer['button_name'];?>"><?php echo $footer['button_name'];?></span>
                        </a>
                    </div>
                <?php } ?>
            </div>
        </div>
    </section>
</main>
<div class="footer-spacing"></div>
<footer class="footer">
    <?php get_template_part_with_data('templates/v-footer-logo');?>

    <?php $socials = array('media'); get_template_part_with_data('templates/v_social', $socials); ?>

    <?php $footer =  get_field('footer-page'); ?>
    <?php if ( !empty($footer) ) get_template_part_with_data('templates/v_footer', $footer); ?>
    <?php /*
    <?php $footer =  get_field('footer-page');?>
    <h4><?php echo $footer['label'];?></h4>
    <h2><?php echo $footer['title'];?></h2>

    <?php if ( !empty($footer['button_name']) ) {?>
        <a href="#" class="btn-contact">
            <span data-hover="<?php echo $footer['button_name'];?>"><?php echo $footer['button_name'];?></span>
        </a>
    <?php }
    if ( !empty($footer['second_button']) && !empty($footer['button_name_2']) ) {?>
        <a href="#" class="btn-contact">
            <span data-hover="<?php echo $footer['button_name_2'];?>"><?php echo $footer['button_name_2'];?></span>
        </a>
    <?php }?>

