<?php $socials = get_field('social', 'option'); ?>
<ul class="social-<?php echo $data[0];?>">
    <?php foreach ($socials as $social) { ?>
    <li>
        <a href="<?php echo $social['url'];?>" target="_blank" rel="nofollow">
            <?php if ( !empty($social['ico']['url']) ) {?>
            <img src="<?php echo $social['ico']['url']?>" alt="<?php echo $social['name'];?>">
            <?php } else {?>
            <?php echo $social['name'];?>
            <?php }?>
        </a>
    </li>
    <?php }?>
</ul>