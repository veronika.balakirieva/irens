<div class="side-image-content">
    <div class="inner">
        <figure class="reveal-effect se2-white wow main-image" data-aos="slide-effect">
            <img src="<?php echo $data[0]['big_image']['url'];?>" alt="Image">
        </figure>
        <figure class="reveal-effect se2-white wow sub-image" data-aos="slide-effect">
            <img src="<?php echo $data[0]['small_image']['url'];?>" alt="Image">
        </figure>
    </div>
    <div class="inner bg1 wow fadeInRight c-w">
        <div class="contenty">
            <h5><?php echo $data[0]['label'];?></h5>
            <h2><?php echo $data[0]['name'];?></h2>
            <p><?php echo $data[0]['text'];?></p>
            <?php if ( !empty($data[0]['link']['url']) ) {?>
            <a href="<?php echo $data[0]['link']['url'];?>">
                <span data-hover="<?php echo $data[0]['link']['title'];?>"><?php echo $data[0]['link']['title'];?></span>
            </a>
            <?php }?>
            <?php if ( $data[1]['label'] != '' ){?>
            <div class="buttons">
                <a data-toggle="modal" data-target="#m1" href="#" class="btn-contact">
                    <span data-hover="<?php echo $data[1]['button_name'];?>"><?php echo $data[1]['button_name'];?></span>
                </a>
            </div>
            <?php }?>
        </div>
    </div>
</div>